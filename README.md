# Desafio Front-end Previdenciarista

Projeto relizado para desafio do processo seletivo da Previdenciarista


## Visão Geral

### Desafio

Usuarios poderão:

- Fazer login com email e senha em formatos válidos
- Acessar informações do usuário clicando sob seu avatar ( ou no icone de hamburguer em resolução mobile)
- Visualizar informações dos módulos
- Visualizar cards com informações simplificadas de petições
- Ao clicar em "Pré-Visualizar" conseguirá ver informações mais detalhadas das respectivas petições;
- Navegar entra as paginas de Petições
- Logout


### Links

- repositório URL: [Clique aqui para ir ao repositório do desafio](https://bitbucket.org/previdenciarista/test-frontend/src/master/)

## Meu Processo:

### Construção

 - O projeto foi realizado utilizando com mobile first, assim tendo uma boa responsividade para diferentes tamanhos de telas.
 - Com componentes separados em diretorios com seus respectivos arquivos de estilização para uma mais facil eventual mudança.
 - A lógica de cada componente foi separada em hooks personalizados, cada hook tenta tem uma função exclusiva, podendo assim reutilizalo em uma eventual expansão da aplicação.
 - As paginas não contém lógica, apenas renderizam componentes que por sua vez contém todo javascript.


### Feito em React.js com

- HTML semântico
- Styled components
- AntDesign
- ContextAPI
- ESLint AirBnb
- axios
- react-icons
- sweetAlert
- jwt-decode
- moment
- react-router-dom


### Continuando a Aplicação

#### Testes:
 - Os testes unitários estão em progresso. Por conta da atualização no react-router-dom a renderização dos componentes estão diferentes do acostumado e estou em etapa de estudos para aprender e me adaptar à nova maneira de testar.
 - Os testes de integração estão em progresso. Ja realizado os testes para Tela de Login, estou aprendendo a renderizar um componente único com um Context.Provider para testar o dashboard.
 

## Autor

- Linkedin - [@lucasfernandesreis](https://www.linkedin.com/in/lucasfernandesreis/)
- GitHub - [@lucasfernandes20](https://github.com/lucasfernandes20)



### Screenshot

  <img src="https://user-images.githubusercontent.com/82236429/153326165-5017375d-a277-4dc8-a030-157df2492234.png" width="150" alt="Login Page" /><br>
  <img src="https://user-images.githubusercontent.com/82236429/153326239-a42d83f4-18d8-41a9-ac82-cf72dad7e5ef.png" width="150" alt="Dashboard Page" /><br>
  <img src="https://user-images.githubusercontent.com/82236429/153326247-e1d3b0be-380c-44c0-819a-cba2dfcaad98.png" width="150" alt="Petition Modal" /><br>
  <img src="https://user-images.githubusercontent.com/82236429/153326257-9d284930-37c8-4abb-851e-b83a3aaf74a7.png" width="150" alt="User Modal" /><br>
  <img src="https://user-images.githubusercontent.com/82236429/153326283-c143554e-e0ba-4073-a35a-1f3f360d4299.png" width="150" alt="Mobile Dashboard" /><br>


