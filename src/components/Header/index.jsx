import React from 'react';
import { HeaderContainer, Exit, ExitIcon } from '../../globalStyles/styles';
import {
  Logo, Flex, HamburguerMenu, ProfilePic, Div,
} from './styles';
import SideBar from '../SideBar';
import useHeader from '../../hooks/useHeader';
import UserModal from '../UserModal';
import useModal from '../../hooks/useModal';
import useLogout from '../../hooks/useLogout';

function Header() {
  const { sideBar, setSideBar, userInfo } = useHeader();
  const {
    showModal, handleCancel, handleOk, isModalVisible,
  } = useModal();
  const { logout } = useLogout();

  return (
    <HeaderContainer>
      <Flex>
        <Logo src="./svgexport-1.svg" alt="Logo" />
        <Div>
          <Exit onClick={() => logout()} header>
            <ExitIcon />
            SAIR
          </Exit>
          <ProfilePic src={userInfo.urlImagemPerfil} alt="Profile" onClick={showModal} />
        </Div>
        <UserModal
          isModalVisible={isModalVisible}
          handleOk={handleOk}
          handleCancel={handleCancel}
        />
        <HamburguerMenu onClick={() => setSideBar(!sideBar)} />
        <SideBar open={sideBar} setSideBar={setSideBar} userInfo={userInfo} />
      </Flex>
    </HeaderContainer>
  );
}

export default Header;
