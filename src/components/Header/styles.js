import styled from 'styled-components';
import { GiHamburgerMenu } from 'react-icons/gi';

export const Logo = styled.img`
  height: 30px;
  @media(min-width: 768px) {
    height: 40px;
    cursor: pointer;
  }
`;

export const Flex = styled.div`
  width: 100%;
  height: 100%;
  padding: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media(min-width: 768px) {
    padding: 0 60px;
  }
`;

export const HamburguerMenu = styled(GiHamburgerMenu)`
  width: 30px;
  height: 30px;
  @media(min-width: 768px) {
    display: none;
  }
`;

export const ProfilePic = styled.img`
    display: none;
  @media(min-width: 768px) {
    display: block;
    width: 80px;
    height: 80px;
    border-radius: 50px;
    padding: 10px;
    object-fit: cover;
    cursor: pointer;
  }
`;

export const Div = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  gap: 20px;
`;
