import styled from 'styled-components';

export const CardContainer = styled.div`
  width: 45%;
  padding-top: 40%;
  position: relative;
  box-shadow: 0px 1px 15px rgba(99, 99, 99, 0.2);
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #FFFF;
  @media(min-width: 768px) {
    width: 23%;
    height: 180px;
    padding-top: 10%;
  }
`;

export const Flex = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  padding: 20px;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  @media(min-width: 768px) {
    padding: 20px;
  }
`;

export const TypeIcon = styled.div`
  width: 10vw;
  height: 10vw;
  background-color: ${(props) => props.background};
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  svg {
    width: 80%;
    height: 80%;
    color: ${(props) => props.color};
  }
  @media(min-width: 768px) {
    width: 35px;
    height: 35px;
  }
`;

export const TypeName = styled.div`
  width: 100%;
  height: 30%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  gap: 4%;
  h3 {
    font-size: 3.3vw;
    font-weight: 500;
    margin: 0;
  }
  @media(min-width: 768px) {
    h3 {
    font-size: 15px;
    }
  }
  @media(min-width: 1024px) {
    h3 {
    font-size: 20px;
    }
  }
`;

export const ModelInfo = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: 1vw;
  @media(min-width: 768px) {
    gap: 10px;
  }
  `;

export const Total = styled.h4`
  font-size: 7vw;
  margin: 0;
  font-weight: 500;
  color: ${(props) => props.color};
  @media(min-width: 768px) {
    font-size: 50px;
  }
  @media(min-width: 1024px) {
    font-size: 50px;
  }
`;

export const CurrentMonth = styled.span`
  font-size: 3vw;
  color: #949595;
  @media(min-width: 768px) {
    font-size: 16px;
  }
  @media(min-width: 1024px) {
    font-size: 19px;
  }
`;
