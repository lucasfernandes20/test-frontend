import PropTypes from 'prop-types';
import React from 'react';

import {
  CardContainer, Flex, TypeIcon, TypeName, ModelInfo, Total, CurrentMonth,
} from './styles';
import useFillCounters from '../../hooks/useFillCounters';
import useCustomizeIcons from '../../hooks/useCustomizeIcons';

function CounterCard({ counter }) {
  const { counterInfo } = useFillCounters(counter);
  const { selectIconProps, selectIcon } = useCustomizeIcons();

  return (
    <CardContainer>
      <Flex>
        <TypeName>
          <TypeIcon
            color={selectIconProps(counterInfo).color}
            background={selectIconProps(counterInfo).background}
          >
            {selectIcon(counterInfo)}
          </TypeIcon>
          <h3>{counterInfo.tipo}</h3>
        </TypeName>
        <ModelInfo>
          <Total color={selectIconProps(counterInfo).color}>{counterInfo.total}</Total>
          {
            counterInfo.totalPeriodo ? (
              <CurrentMonth>{`Este Mês: ${counterInfo.totalPeriodo.mensal}`}</CurrentMonth>
            ) : null
          }
        </ModelInfo>
      </Flex>
    </CardContainer>
  );
}

CounterCard.propTypes = {
  counter: PropTypes.string.isRequired,
};

export default CounterCard;
