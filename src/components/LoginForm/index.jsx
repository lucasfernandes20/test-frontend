import React from 'react';
import {
  LoginContainer,
  FormContainer,
  Logo,
  Label,
  Input,
  SubmitButton,
  ForgotPassword,
  InputContainer,
  Warning,
} from './styles';
import { PaperContainer } from '../../globalStyles/styles';
import useLogin from '../../hooks/useLogin';

function LoginForm() {
  const {
    validateData, handleChange, invalidField, login,
  } = useLogin();

  return (
    <LoginContainer>
      <PaperContainer>
        <FormContainer>
          <Logo src="./svgexport-1.svg" alt="Logo" />
          <InputContainer>
            <Label htmlFor="input-text">
              E-mail ou CPF
              <Input
                id="input-text"
                type="text"
                placeholder="Digite seu e-mail ou CPF"
                name="email"
                value={login.email}
                onChange={({ target }) => handleChange(target)}
              />
            </Label>
            <Label htmlFor="input-pass">
              senha
              <Input
                id="input-pass"
                type="password"
                placeholder="Digite sua senha"
                name="password"
                value={login.password}
                onChange={({ target }) => handleChange(target)}
              />
            </Label>
            <ForgotPassword>Esqueceu a senha?</ForgotPassword>
          </InputContainer>
          <SubmitButton
            type="button"
            onClick={() => validateData()}
            disabled={!login.email || !login.password || login.password.length < 6}
          >
            Entrar
          </SubmitButton>
          <Warning valid={invalidField}>Email ou senha invalidos</Warning>
        </FormContainer>
      </PaperContainer>
    </LoginContainer>
  );
}

export default LoginForm;
