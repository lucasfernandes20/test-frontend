import styled from 'styled-components';

export const LoginContainer = styled.section`
  width: 100%;
  height: 100vh;
  padding: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  overflow-x: hidden;
`;

export const FormContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media(min-width: 768px) {
    align-items: flex-start;
  }
`;

export const Logo = styled.img`
  width: 200px;
  align-self: center;
  @media(min-width: 768px) {
    width: 300px;
    align-self: flex-start;
  }
`;

export const Label = styled.label`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  color: gray;
  font-weight: 400;
  @media(min-width: 768px) {
    gap: 10px;
  }
`;

export const Input = styled.input`
  border: 1px solid #949595;
  height: 40px;
  width: 100%;
  border-radius: 3px;
  padding-left: 10px;
  @media(min-width: 768px) {
    height: 50px;
  }
`;

export const SubmitButton = styled.button`
  width: 100%;
  height: 40px;
  border: none;
  background-color: ${(props) => (props.disabled ? '#bab9b8' : '#F26526')};
  border-radius: 5px;
  color: #FFFF;
  font-weight: 600;
  font-size: 18px;
  cursor: pointer;
  @media(min-width: 768px) {
    height: 50px;
  }
`;

export const ForgotPassword = styled.p`
  color: #3575AB;
  font-size: 12px;
  font-weight: 600;
  cursor: pointer;
`;

export const InputContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

export const Warning = styled.p`
  font-size: 13px;
  animation: ${(props) => (props.valid ? 'shake 0.82s cubic-bezier(.36,.07,.19,.97) both' : null)};
  transform: translate3d(0, 0, 0);
  color: red;
  visibility: ${(props) => (props.valid ? 'visible' : 'hidden')};
`;
