import styled from 'styled-components';

export const CounterListSection = styled.section`
  width: 100%;
  padding: 10px;
  max-width: 1024px;
  margin: 30px auto auto auto;
  @media(min-width: 1034px) {
    padding: 10px 0;
  }
`;

export const Flex = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  gap: 4vw;
  align-items: flex-end;
  justify-content: space-between;
  @media(min-width: 768px) {
    flex-direction: row;
    gap: 0;
    justify-content: space-between;
  }
`;
