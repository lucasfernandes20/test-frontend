import React, { useContext } from 'react';
import { CounterListSection, Flex } from './styles';
import { UserContext } from '../../Context/Provider';
import CounterCard from '../CounterCard';

function CounterList() {
  const { userInfo } = useContext(UserContext);

  return (
    <CounterListSection>
      <Flex>
        {
        userInfo.modulos
          ? userInfo.modulos.map((e) => <CounterCard key={e} counter={e} />) : null
        }
      </Flex>
    </CounterListSection>
  );
}

export default CounterList;
