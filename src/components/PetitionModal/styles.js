import styled from 'styled-components';

export const Container = styled.section`
  width: 100%;
  padding: 10px;
`;

export const Resume = styled.p`
  font-size: 17px;
  color: gray;
  font-weight: 800;
  margin: 40px 0 80px 0;
`;

export const InfoContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  gap: 6px;
`;

export const InfoIten = styled.h5`
  font-size: 14px;
  font-weight: 700;
  color: #6838B7;
  display: flex;
  align-items: center;
  gap: 10px;
  background-color: #EBE5F6;
  padding: 4px 10px;
  border-radius: 4px;
  @media(min-width: 768px) {
  font-size: 17px;
  }
`;

export const AuthorContainer = styled.div`
  width: 100%;
  margin-top: 80px;
  display: flex;
  align-items: center;
  gap: 4px;
  flex-direction: column;
  @media(min-width: 768px) {
  font-size: 17px;
  flex-direction: row;
  }
`;

export const AuthorEmail = styled.p`
  font-size: 16px;
  font-weight: 800;
  color: gray;
`;

export const FeedbackContainer = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
`;

export const LikesAndDislikes = styled.p`
  display: flex;
  align-items: center;
  color: #00A881;
  font-size: 17px;
  gap: 4px;
  padding: 7px;
  border-radius: 4px;
  background-color: #E5F6F2;
  cursor: pointer;
`;
