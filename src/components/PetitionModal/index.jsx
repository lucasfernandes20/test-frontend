import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Avatar } from 'antd';
import { SiProcessingfoundation } from 'react-icons/si';
import { GiJusticeStar, GiPointing } from 'react-icons/gi';
import { BiSearchAlt } from 'react-icons/bi';
import { BsCalendarWeek } from 'react-icons/bs';
import { AiOutlineUser, AiOutlineLike, AiOutlineDislike } from 'react-icons/ai';
import {
  Container, Resume, InfoContainer,
  InfoIten, AuthorContainer, AuthorEmail,
  FeedbackContainer, LikesAndDislikes,
} from './styles';
import {
  Tag, TagsContainer,
  SubType, CardFooter, Puclished, Title,
} from '../PetitionCard/styles';
import textConverter from '../../utils/textConverter';
import formatBrazilDate from '../../utils/formatBrazilDate';

function PetitionModal({
  isModalVisible, handleOk, handleCancel, content,
}) {
  if (content) {
    return (
      <Modal
        title="Informações pessoais"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
      >
        <Container>
          <TagsContainer>
            {content.tags
              ? content.tags.map((tag) => (

                <Tag key={tag}>
                  {textConverter(tag)}
                </Tag>
              )) : null}
          </TagsContainer>
          {
            content.subtipo && content.subtipo[0]
              ? <SubType>{textConverter(content.subtipo[0])}</SubType>
              : null
          }
          <Title>{content.titulo}</Title>
          <Resume>{content.resumo}</Resume>
          <InfoContainer>
            {
              content.status
                ? (
                  <InfoIten>
                    <BiSearchAlt />
                    {`Status: ${textConverter(content.status)}`}
                  </InfoIten>
                ) : null
            }
            {
            content.tipoDeProcesso
              ? (
                <InfoIten>
                  <SiProcessingfoundation />
                  {`Tipo de processo: ${textConverter(content.tipoDeProcesso)}`}
                </InfoIten>
              ) : null
            }
            {
              content.tipoDeAcao
                ? (
                  <InfoIten>
                    <GiJusticeStar />
                    Tipo de ação:
                    <span>{textConverter(content.tipoDeAcao)}</span>
                  </InfoIten>
                ) : null
            }
            {
              content.competencia
                ? (
                  <InfoIten>
                    <GiPointing />
                    Competência:
                    <span>{textConverter(content.competencia)}</span>
                  </InfoIten>
                ) : null
            }
            {
              content.carenciaMinima
                ? (
                  <InfoIten>
                    <BsCalendarWeek />
                    Carência mínima:
                    <span>{content.carenciaMinima}</span>
                  </InfoIten>
                ) : null

            }
          </InfoContainer>
          <AuthorContainer>
            <Avatar style={{ backgroundColor: '#87d068' }} icon={<AiOutlineUser />} />
            <AuthorEmail>{content.criadoPor || 'Anônimo'}</AuthorEmail>
          </AuthorContainer>
          <CardFooter>
            <Puclished>{`Publicação: ${formatBrazilDate(content.dataDeCriacao)}`}</Puclished>
            <FeedbackContainer>
              <LikesAndDislikes>
                <AiOutlineLike />
                {content.curtidas || 0}
              </LikesAndDislikes>
              <LikesAndDislikes>
                <AiOutlineDislike />
                {content.criticas || 0}
              </LikesAndDislikes>
            </FeedbackContainer>
          </CardFooter>
        </Container>
      </Modal>
    );
  }
  return (
    <Modal
      title="Informações pessoais"
      visible={isModalVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={null}
    >
      <h1>Carregando...</h1>
    </Modal>
  );
}

PetitionModal.propTypes = {
  handleCancel: PropTypes.func.isRequired,
  handleOk: PropTypes.func.isRequired,
  isModalVisible: PropTypes.bool.isRequired,
  content: PropTypes.shape({
    id: PropTypes.string,
    titulo: PropTypes.string,
    resumo: PropTypes.string,
    tipo: PropTypes.string,
    status: PropTypes.string,
    sexo: PropTypes.string,
    tipoDeProcesso: PropTypes.string,
    tipoDeAcao: PropTypes.string,
    tiposDeBeneficio: PropTypes.arrayOf(PropTypes.string),
    subtipo: PropTypes.arrayOf(PropTypes.string),
    competencia: PropTypes.string,
    carenciaMinima: PropTypes.number,
    slug: PropTypes.string,
    tags: PropTypes.arrayOf(PropTypes.string),
    criadoPor: PropTypes.string,
    atualizadoPor: PropTypes.string,
    curtidas: PropTypes.number,
    criticas: PropTypes.number,
    idadeMinima: PropTypes.number,
    ativa: PropTypes.bool,
    score: PropTypes.number,
    profissoes: PropTypes.arrayOf(PropTypes.shape({
      codigo: PropTypes.string,
      titulo: PropTypes.string,
      sinonimos: PropTypes.arrayOf(PropTypes.string),
    })),
    incapacidades: PropTypes.arrayOf(PropTypes.string),
    dataDeCriacao: PropTypes.string,
    dataDaUltimaAtualizacao: PropTypes.string,
    periodo: PropTypes.string,
    highlights: PropTypes.arrayOf(PropTypes.string),
  }),
};

PetitionModal.defaultProps = {
  content: {},
};

export default PetitionModal;
