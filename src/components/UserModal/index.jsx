import PropTypes from 'prop-types';
import React, { useContext } from 'react';
import { Modal } from 'antd';
import { UserContext } from '../../Context/Provider';
import {
  Container, Profile, Apresentation, Name, Infos,
} from './styles';

function UserModal({ isModalVisible, handleOk, handleCancel }) {
  const { userInfo } = useContext(UserContext);

  if (userInfo.privilegio) {
    return (
      <Modal
        title="Informações pessoais"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
      >
        <Container>
          <Apresentation>
            <Profile src={userInfo.urlImagemPerfil} alt="profile" />
            <Name>{userInfo.nome}</Name>
          </Apresentation>
          <Infos>
            <p>
              E-mail:
              {' '}
              <span>{userInfo.email}</span>
            </p>
            <p>
              Privilégio:
              {' '}
              <span>{userInfo.privilegio.toLowerCase()}</span>
            </p>
          </Infos>
        </Container>
      </Modal>
    );
  }
  return (
    <Modal
      title="Informações pessoais"
      visible={isModalVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={null}
    >
      <h1>Carregando...</h1>
    </Modal>
  );
}

UserModal.propTypes = {
  handleCancel: PropTypes.func.isRequired,
  handleOk: PropTypes.func.isRequired,
  isModalVisible: PropTypes.bool.isRequired,
};

export default UserModal;
