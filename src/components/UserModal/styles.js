import styled from 'styled-components';

export const Container = styled.div`
  padding: 30px;
`;

export const Profile = styled.img`
  width: 100px;
  height: 100px;
  object-fit: cover;
  border-radius: 5px;
`;

export const Apresentation = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  gap: 40px;
  margin-bottom: 50px;
`;

export const Name = styled.h3`
  font-size: 25px;
  text-align: left;
  font-weight: 800;
`;

export const Infos = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: 15px;
  p {
    font-size: 14px;
    font-weight: 500;
    span {
      font-size: 16px;
      font-weight: 800;
      color: gray;
    }
  }
`;
