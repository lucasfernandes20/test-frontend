import styled from 'styled-components';

export const PetitionsContainer = styled.section`
  width: 100vw;
  padding: 10px;
  max-width: 1024px;
  margin: 50px auto auto auto;
  h1 {
    font-size: 25px;
    font-weight: 800;
  }
  @media(min-width: 1034px) {
    padding: 1px;
  }
`;

export const PetitionsListContainer = styled.div`
  width: 100%;
  display: flex;
  gap: 25px;
  flex-direction: column;
  margin-bottom: 50px;
  align-items: flex-end;
`;
