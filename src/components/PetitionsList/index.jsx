import React from 'react';
import { Pagination } from 'antd';
import { PetitionsContainer, PetitionsListContainer } from './styles';
import PetitionCard from '../PetitionCard';
import PetitionModal from '../PetitionModal';
import useModal from '../../hooks/useModal';
import useFillPetitions from '../../hooks/useFillPetitions';

function Petitions() {
  const {
    showModal, handleCancel, handleOk, isModalVisible, modalContent,
  } = useModal();
  const { petitions, setPage, maxPage } = useFillPetitions();

  return (
    <PetitionsContainer>
      <h1>Últimas petições</h1>
      <PetitionsListContainer>
        {
          petitions
            ? petitions.map((petition) => (
              <PetitionCard
                key={petition.id}
                petition={petition}
                showModal={showModal}
              />
            ))
            : null
        }
        <Pagination
          size={window.innerWidth < 768 ? 'small' : 'medium'}
          onChange={(current) => setPage(current)}
          defaultCurrent={1}
          total={maxPage}
          defaultPageSize={2}
          style={{ marginTop: '40px' }}
        />
      </PetitionsListContainer>
      <PetitionModal
        content={modalContent}
        isModalVisible={isModalVisible}
        showModal={showModal}
        handleCancel={handleCancel}
        handleOk={handleOk}
      />
    </PetitionsContainer>
  );
}

export default Petitions;
