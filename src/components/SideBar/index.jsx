import PropTypes from 'prop-types';
import React from 'react';
import useLogout from '../../hooks/useLogout';
import {
  SideBarContainer, CloseIcon, Flex, Exit, ExitIcon, ProfilePic, Info,
} from './styles';

function SideBar({ open, setSideBar, userInfo }) {
  const { logout } = useLogout();
  return (
    <SideBarContainer isOpen={open}>
      <CloseIcon onClick={() => setSideBar(false)} />
      <Flex>
        <ProfilePic src={userInfo.urlImagemPerfil} alt="profile" />
        <Info>{userInfo.nome}</Info>
        <Info>{userInfo.email}</Info>
        <Info>{userInfo.privilegio}</Info>
      </Flex>
      <Exit onClick={() => logout()}>
        <ExitIcon />
        SAIR
      </Exit>
    </SideBarContainer>
  );
}

SideBar.propTypes = {
  open: PropTypes.bool.isRequired,
  setSideBar: PropTypes.func.isRequired,
  userInfo: PropTypes.shape({
    ativo: PropTypes.bool,
    email: PropTypes.string,
    modulos: PropTypes.arrayOf(PropTypes.string),
    nome: PropTypes.string,
    privilegio: PropTypes.string,
    urlImagemPerfil: PropTypes.string,
    uuid: PropTypes.string,
  }).isRequired,
};

export default SideBar;
