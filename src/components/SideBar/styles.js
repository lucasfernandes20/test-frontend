import styled from 'styled-components';
import { CgClose } from 'react-icons/cg';
import { BsArrowBarLeft } from 'react-icons/bs';

export const SideBarContainer = styled.aside`
  position: fixed;
  top: 0;
  animation: ${(props) => (props.isOpen ? 'rightAppaer 500ms both' : null)};
  display: ${(props) => (props.isOpen ? 'flex' : 'none')};
  box-shadow: ${(props) => (props.isOpen ? '1px 2px 10px rgba(0, 0, 0, 0.4)' : null)};
  right: 0;
  width: 60vw;
  height: 100vh;
  padding: 10px;
  background-color: #FFFF;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
  z-index: 1000;
  @media(min-width: 768px) {
    display: none;
  }
`;

export const CloseIcon = styled(CgClose)`
  width: 30px;
  height: 30px;
`;

export const Exit = styled.h3`
  font-size: 20px;
  font-weight: 100;
  display: flex;
  align-items: center;
`;

export const ExitIcon = styled(BsArrowBarLeft)`
  width: 20px;
  height: 20px;
`;

export const Flex = styled.div`
  width: 100%;
  height: 50%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-evenly;
  margin-left: -10px;
`;

export const ProfilePic = styled.img`
  width: 100px;
  height: 100px;
  object-fit: cover;
  border-radius: 50px;
  align-self: center;
`;

export const Info = styled.p`
  font-size: 13px;
  font-weight: 800;
  color: #F26526;
  border-left: 5px solid #F26526;
  padding-left: 10px;
`;
