import PropTypes from 'prop-types';
import React from 'react';
import { AiOutlineEye } from 'react-icons/ai';
import {
  CardContainer, Tag, TagsContainer,
  SubType, CardFooter, Puclished, Preview, Title,
} from './styles';
import textConverter from '../../utils/textConverter';
import formatBrazilDate from '../../utils/formatBrazilDate';

function PetitionCard({ petition, showModal }) {
  return (
    <CardContainer>
      <TagsContainer>
        {petition.tags
          ? petition.tags.map((tag) => (

            <Tag key={tag}>
              {textConverter(tag)}
            </Tag>
          )) : null}
      </TagsContainer>
      {
        petition.subtipo && petition.subtipo[0]
          ? <SubType>{textConverter(petition.subtipo[0])}</SubType> : null
      }
      <Title>{petition.titulo}</Title>
      <CardFooter>
        <Puclished>{`Publicação: ${formatBrazilDate(petition.dataDeCriacao)}`}</Puclished>
        <Preview onClick={() => showModal(petition)}>
          <AiOutlineEye />
          {' '}
          Pré-visualizar Petição
        </Preview>
      </CardFooter>
    </CardContainer>
  );
}

PetitionCard.propTypes = {
  showModal: PropTypes.func.isRequired,
  petition: PropTypes.shape({
    id: PropTypes.string,
    titulo: PropTypes.string,
    resumo: PropTypes.string,
    tipo: PropTypes.string,
    status: PropTypes.string,
    sexo: PropTypes.string,
    tipoDeProcesso: PropTypes.string,
    tipoDeAcao: PropTypes.string,
    tiposDeBeneficio: PropTypes.arrayOf(PropTypes.string),
    subtipo: PropTypes.arrayOf(PropTypes.string),
    competencia: PropTypes.string,
    carenciaMinima: PropTypes.number,
    slug: PropTypes.string,
    tags: PropTypes.arrayOf(PropTypes.string),
    criadoPor: PropTypes.string,
    atualizadoPor: PropTypes.string,
    curtidas: PropTypes.number,
    criticas: PropTypes.number,
    idadeMinima: PropTypes.number,
    ativa: PropTypes.bool,
    score: PropTypes.number,
    profissoes: PropTypes.arrayOf(PropTypes.shape({
      codigo: PropTypes.string,
      titulo: PropTypes.string,
      sinonimos: PropTypes.arrayOf(PropTypes.string),
    })),
    incapacidades: PropTypes.arrayOf(PropTypes.string),
    dataDeCriacao: PropTypes.string,
    dataDaUltimaAtualizacao: PropTypes.string,
    periodo: PropTypes.string,
    highlights: PropTypes.arrayOf(PropTypes.string),
  }),
};

PetitionCard.defaultProps = {
  petition: {},
};

export default PetitionCard;
