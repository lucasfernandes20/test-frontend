import styled from 'styled-components';

export const CardContainer = styled.div`
  width: 100%;
  background-color: #FFFF;
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: 10px;
  box-shadow: 0px 1px 15px rgba(99, 99, 99, 0.2);
  border-radius: 4px;
`;

export const Tag = styled.h5`
  font-size: 18px;
  padding: 5px;
  color: #3575AB;
  background-color: #EDF6FD;
  font-weight: 700;
  border-radius: 4px;
`;

export const TagsContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  gap: 10px;
  margin-bottom: 20px;
`;

export const SubType = styled.h3`
  font-size: 20px;
  font-weight: 800;
  color: #F26526;
`;

export const Title = styled.h5`
  font-size: 16px;
  font-weight: 600;
`;

export const CardFooter = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  margin-top: 20px;
  gap: 20px;
  @media(min-width: 768px) {
    flex-direction: row;
    align-items: flex-end;
  }
`;

export const Puclished = styled.p`
  font-size: 15px;
  color: #3575AB;
  @media(min-width: 768px) {
  font-size: 18px;
  }
`;

export const Preview = styled.p`
    font-size: 18px;
    color: #3575AB;
    display: flex;
    align-items: center;
    gap: 8px;
    cursor: pointer;
    svg {
      width: 20px;
      height: 20px;
    }
`;
