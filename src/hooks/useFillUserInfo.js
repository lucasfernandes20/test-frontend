import { useState } from 'react';
import decodeToken from '../utils/decodeToken';

const usefillUserInfo = () => {
  const [userInfo, setUserInfo] = useState({});

  const getUserInfo = () => {
    const decodedToken = decodeToken();
    return setUserInfo(() => decodedToken);
  };

  return { getUserInfo, userInfo };
};

export default usefillUserInfo;
