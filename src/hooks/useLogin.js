import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import Swal from 'sweetalert2';
import loginUser from '../services/loginUser';

const useLogin = () => {
  const [login, setLogin] = useState({ email: '', password: '' });
  const [invalidField, setInvalidField] = useState(false);

  const navigate = useNavigate();

  const handleChange = (elm) => {
    const { name, value } = elm;
    setLogin((prevState) => ({ ...prevState, [name]: value }));
  };

  const submitUser = async () => {
    const { error } = await loginUser();

    if (error) {
      return (
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo deu errado ao tentar fazer login. Por favor tente novamente mais tarde!',
        })
      );
    }

    return navigate('/dashboard');
  };

  const validateData = () => {
    const emailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    if (!login.email || !emailRegex.test(login.email)) return setInvalidField(true);

    if (!login.password || login.password.length < 6) return setInvalidField(true);

    return submitUser();
  };

  return {
    validateData, handleChange, invalidField, login,
  };
};

export default useLogin;
