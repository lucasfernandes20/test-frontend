import React from 'react';
import {
  AiOutlineUser, AiOutlineFileText, AiOutlineCalculator, AiOutlineFolder,
} from 'react-icons/ai';

const useCustomizeIcons = () => {
  const selectIconProps = (counterInfo) => {
    switch (counterInfo.tipo) {
      case 'CLIENTE':
        return { color: '#1B77C5', background: '#0081F61F' };
      case 'PETICOES':
        return { color: '#FF8853', background: '#FEF0E9' };
      case 'CALCULOS':
        return { color: '#00A881', background: '#E5F6F2' };
      case 'CASOS':
        return { color: '#6838B7', background: '#EBE5F6' };
      default:
        return { color: '#1B77C5', background: '#0081F61F' };
    }
  };

  const selectIcon = (counterInfo) => {
    switch (counterInfo.tipo) {
      case 'CLIENTE':
        return <AiOutlineUser />;
      case 'PETICOES':
        return <AiOutlineFileText />;
      case 'CALCULOS':
        return <AiOutlineCalculator />;
      case 'CASOS':
        return <AiOutlineFolder />;
      default:
        return <AiOutlineUser />;
    }
  };

  return { selectIconProps, selectIcon };
};

export default useCustomizeIcons;
