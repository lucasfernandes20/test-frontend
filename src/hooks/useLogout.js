import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

const useLogout = () => {
  const navigate = useNavigate();

  const logout = () => {
    Swal.fire({
      title: 'Deseja fazer logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não',
    }).then((result) => {
      if (result.isConfirmed) {
        localStorage.removeItem('token');

        navigate('/login');
      }
    });
  };

  return { logout };
};

export default useLogout;
