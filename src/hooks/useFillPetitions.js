import { useEffect, useState } from 'react';
import getPetitions from '../services/getPetitions';

const useFillPetitions = () => {
  const [petitions, setPetitions] = useState([]);
  const [page, setPage] = useState(1);
  const [maxPage, setMaxPage] = useState();

  const setCurrentPetitions = async () => {
    const { data, headers } = await getPetitions(page);
    const total = headers['x-total-count'];
    setMaxPage(total);
    return setPetitions(data);
  };

  useEffect(() => {
    setCurrentPetitions();
  }, [page]);

  return { petitions, setPage, maxPage };
};

export default useFillPetitions;
