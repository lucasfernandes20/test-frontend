import { useState, useEffect } from 'react';
import getCounters from '../services/getCounter';

const useFillCounters = (type) => {
  const [counterInfo, setCounterInfo] = useState({});

  const getCountersInfo = async () => {
    const data = await getCounters(type);
    return setCounterInfo(() => data);
  };

  useEffect(() => {
    getCountersInfo();
  }, []);

  return { counterInfo };
};

export default useFillCounters;
