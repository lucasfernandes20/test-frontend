import { useContext, useEffect, useState } from 'react';
import { UserContext } from '../Context/Provider';

const useHeader = () => {
  const [sideBar, setSideBar] = useState(false);

  const { getUserInfo, userInfo } = useContext(UserContext);

  useEffect(() => {
    getUserInfo();
  }, []);

  return { sideBar, setSideBar, userInfo };
};

export default useHeader;
