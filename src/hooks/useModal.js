import { useState } from 'react';

const useProfileModal = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalContent, setModalContent] = useState({});

  const showModal = (content) => {
    setIsModalVisible(true);
    if (content) setModalContent(content);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return {
    showModal, handleCancel, handleOk, isModalVisible, modalContent,
  };
};

export default useProfileModal;
