import React from 'react';
import { MainContainer } from '../globalStyles/styles';
import Header from '../components/Header';
import CounterList from '../components/CountersList';
import PetitionsList from '../components/PetitionsList';

function DashBoard() {
  return (
    <MainContainer>
      <Header />
      <CounterList />
      <PetitionsList />
    </MainContainer>
  );
}

export default DashBoard;
