import React from 'react';
import LoginForm from '../components/LoginForm';
import { MainContainer } from '../globalStyles/styles';

function LoginPage() {
  return (
    <MainContainer>
      <LoginForm />
    </MainContainer>
  );
}

export default LoginPage;
