import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

function Wrapper({ children }) {
  const history = createMemoryHistory();

  return (
    <Router location={history.location} navigator={history} >
      {children}
    </Router>
  );
}

export default Wrapper;
