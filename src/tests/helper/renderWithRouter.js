import React, {createContext} from 'react';
import { createMemoryHistory } from 'history';
import {  MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';

export const UserContext = createContext();

const renderWithRouter = (
  component,
  {
    initialState,
  } = {},
  {
    route = '/',
    history = createMemoryHistory(),
  } = {},
) => ({
  ...render(
    <UserContext.Provider value={initialState}>
      {console.log(initialState)}
      <MemoryRouter history={ history }>
        {component}
      </MemoryRouter>
    </UserContext.Provider>
  ),
  history,
});

export default renderWithRouter;