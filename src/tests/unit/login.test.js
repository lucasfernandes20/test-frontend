/* eslint-disable no-undef */
import { renderHook, act } from '@testing-library/react-hooks';
import useLogin from '../../hooks/useLogin';
import Wrapper from '../helper/wrapper'
import axios from 'axios'
import loginMock from '../mocks/loginMock'
import { screen } from "@testing-library/react";

axios.post = jest.fn(async () => loginMock)

describe('Testa hook de Login', () => {

  it('Se o formato do email ou senha for invalido, o estado invalidField é mudado para "true", exibindo uma mensagem de erro', async () => {
    const {result, waitForNextUpdate} = renderHook(() => useLogin(), {wrapper: Wrapper});
    expect(result.current.invalidField).toEqual(false);
    expect(result.current.login).toEqual({email: '', password: ''})
    act(() => {
      result.current.setLogin({email: 'lucas@email.com', password: '1233212'})
    })
    act(() => {
      result.current.validateData()
    })
    expect(result.current.invalidField).toEqual(false);

    act(() => {
      result.current.setLogin({email: 'lucas@email.cooooooooooooom', password: '1233212'})
    })
    act(() => {
      result.current.validateData()
    })

    expect(result.current.invalidField).toEqual(true);
    
    await waitForNextUpdate()
    
    const errMessage = screen.getByText(/email ou senha invalidos/i)

    expect(errMessage).toBeInTheDocument()
  });

  it('o estado do login começa sendo vazio em ambas as chaves', async () => {
    const { result } = renderHook(() => useLogin(), {wrapper: Wrapper});
    expect(result.current.invalidField).toEqual(false);
    expect(result.current.login).toEqual({email: '', password: ''})
  });

  it('Se der erro no login deve ser renderizado um alert dizendo que ocorreu um erro', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useLogin(), {wrapper: Wrapper});
    axios.post = jest.fn(async () => ({error: 'deu um erro'}))
    act(() => {
      result.current.setLogin({email: 'lucas@email.com', password: '1233212'})
    })
    act(() => {
      result.current.validateData()
    })
    expect(result.current.invalidField).toEqual(false);
  })

});
