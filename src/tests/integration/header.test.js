import React, {useContext} from "react";
import { screen, waitFor } from "@testing-library/react";
import renderWithRouter from "../helper/renderWithRouter";
import Header from "../../components/Header";
import userEvent from '@testing-library/user-event';
import useFillUserInfo from '../../hooks/useFillUserInfo'
import userInfoMock from '../mocks/userInfoMock'

describe('Header', () => {

  beforeEach(() => {
    
    renderWithRouter(<Header />, {initialState: {getUserInfo, userInfo: userInfoMock}})

    useContext = jest.fn({getUserInfo: useFillUserInfo, userInfo: useInfoMock})

  })

  it('Verifica se os elementos estão presente no Header', () => {
    const ProfilePic = screen.getByRole('img', {
      name: /profile/i
    })

    expect(ProfilePic).toBeDefined()
  })

})