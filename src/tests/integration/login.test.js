import React from "react";
import { screen, waitFor } from "@testing-library/react";
import renderWithRouter from "../helper/renderWithRouter";
import LoginForm from "../../components/LoginForm";
import userEvent from '@testing-library/user-event';
import loginUser from '../../services/loginUser'
jest.mock('../../services/loginUser')

describe('Login', () => {

  beforeEach(() => {
    loginUser.mockResolvedValue({error: 'erro'})
    
    renderWithRouter(<LoginForm />)

  })

  it('Verifica se os elementos estão presente na Tela de Login', () => {
    const emailInput = screen.getByLabelText(/e\-mail ou cpf/i)

    const passowordInput = screen.getByLabelText(/senha/i)

    const submitButton = screen.getByRole('button', {
      name: /entrar/i
    })
    expect(emailInput).toBeInTheDocument()
    expect(passowordInput).toBeInTheDocument()
    expect(submitButton).toBeInTheDocument()
  })

  it('O botão inicia com a propriedade "disabled" sendo "true"', () => {
    const submitButton = screen.getByRole('button', {
      name: /entrar/i
    })

    expect(submitButton).toHaveProperty('disabled', true)
  })

  it('Ao fazer login com dados em mal formato ou inválidos mostra um aviso de má formatação', async () => {
    const emailInput = screen.getByLabelText(/e\-mail ou cpf/i)

    const passowordInput = screen.getByLabelText(/senha/i)

    const submitButton = screen.getByRole('button', {
      name: /entrar/i
    })

    const emailInputValue = 'lucas@gmail.coooooooooooom'
    const passwordInputValue = 'lucas123'

    userEvent.type(passowordInput, passwordInputValue)
    userEvent.type(emailInput, emailInputValue)

    expect(emailInput.value).toBe(emailInputValue)
    expect(passowordInput.value).toBe(passwordInputValue)
    expect(submitButton).toHaveProperty('disabled', false)

    userEvent.click(submitButton)

    const invalidFieldsMessage = screen.getByText(/email ou senha invalidos/i)

    expect(invalidFieldsMessage).toBeDefined()
  })

  it('Caso aconteça alguma má requisição na hora do login, retorna um alerta avisando do erro', async () => {
    const emailInput = screen.getByLabelText(/e\-mail ou cpf/i)

    const passowordInput = screen.getByLabelText(/senha/i)

    const submitButton = screen.getByRole('button', {
      name: /entrar/i
    })

    const emailInputValue = 'lucas@gmail.com'
    const passwordInputValue = 'lucas123'

    userEvent.type(passowordInput, passwordInputValue)
    userEvent.type(emailInput, emailInputValue)

    expect(emailInput.value).toBe(emailInputValue)
    expect(passowordInput.value).toBe(passwordInputValue)
    expect(submitButton).toHaveProperty('disabled', false)

    userEvent.click(submitButton)

    await waitFor(()=> screen.getByRole('heading', {name: /oops\.\.\./i}))

    expect(screen.getByRole('heading', {name: /oops\.\.\./i})).toBeDefined()

  })
})