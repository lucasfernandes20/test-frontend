module.exports = {
  "ativo": true,
  "privilegio": "PROPRIETARIO",
  "nome": "Jon Snow Frontend",
  "uuid": "bc6fcf7b-9c91-43e3-9dd2-04a1cdc60efc",
  "email": "jonsnow@nightswatch.com",
  "urlImagemPerfil": "https://thispersondoesnotexist.com/image",
  "modulos": [
    "CLIENTES",
    "CALCULOS",
    "PETICOES",
    "CASOS"
  ]
}