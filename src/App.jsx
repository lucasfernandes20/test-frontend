import React from 'react';
import './App.css';
import { Route, Routes, Navigate } from 'react-router-dom';
import LoginPage from './pages/LoginPage';
import DashBoard from './pages/DashBoard';

function App() {
  return (
    <Routes>
      <Route exact path="/login" element={<LoginPage />} />
      <Route exact path="/dashboard" element={<DashBoard />} />
      <Route exact path="/" element={<Navigate to="/login" />} />
    </Routes>
  );
}

export default App;
