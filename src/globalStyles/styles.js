import styled from 'styled-components';
import Paper from '@material-ui/core/Paper';
import { BsArrowBarLeft } from 'react-icons/bs';

export const MainContainer = styled.main`
  width: 100vw;
  min-height: 100vh;
  overflow-x: hidden;
  background-color: whitesmoke;
`;

export const HeaderContainer = styled.header`
  width: 100vw;
  height: 80px;
  background-color: white;
`;

export const PaperContainer = styled(Paper)`
  width: 100%;
  height: 350px;
  padding: 30px;
  @media(min-width: 768px) {
    width: 500px;
    height: 500px;
  }
`;
export const Exit = styled.h3`
  font-size: 15px;
  font-weight: 100;
  display: ${(props) => (props.header ? 'none' : 'flex')};
  align-items: center;
  cursor: pointer;
  @media(min-width: 768px) {
    display: flex;
  }
`;

export const ExitIcon = styled(BsArrowBarLeft)`
  width: 15px;
  height: 15px;
`;
