/* eslint-disable react/jsx-no-constructed-context-values */
import PropTypes from 'prop-types';
import React, { createContext } from 'react';
import usefillUserInfo from '../hooks/useFillUserInfo';

export const UserContext = createContext();

function Provider({ children }) {
  const { getUserInfo, userInfo } = usefillUserInfo();

  const value = { getUserInfo, userInfo };

  return (
    <UserContext.Provider value={value}>
      {children}
    </UserContext.Provider>
  );
}

Provider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Provider;
