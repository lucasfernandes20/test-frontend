import api from './api';

const getCounters = async (modulo) => {
  try {
    const result = await api.get(`/counter?tipo=${modulo}`);

    return result.data[0];
  } catch (err) {
    console.log(err);
    return err;
  }
};

export default getCounters;
