import api from './api';

const getPetitions = async (page) => {
  try {
    const result = await api.get(`/peticoes?_page=${page}&_limit=2`);
    return result;
  } catch (err) {
    return err;
  }
};

export default getPetitions;
