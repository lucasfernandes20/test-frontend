import axios from 'axios';

const loginUser = async () => {
  try {
    const body = { username: 'teste', password: 'frontend' };

    const token = await axios.post('http://localhost:8080/oauth', body);

    localStorage.setItem('token', JSON.stringify(token.data));

    return { data: token.data };
  } catch (err) {
    return { error: err };
  }
};

export default loginUser;
