const textConverter = (string) => {
  const convert = string.split('_')
    .map((word) => word[0].toUpperCase() + word.slice(1).toLowerCase())
    .join(' ');

  return convert;
};

export default textConverter;
