import moment from 'moment';

const formatBrazilDate = (date) => {
  const stringDate = new Date(date);
  const formated = moment(stringDate).format('DD/MM/YYYY - HH:mm');

  return formated;
};

export default formatBrazilDate;
