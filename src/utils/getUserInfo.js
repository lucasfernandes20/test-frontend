const getUserInfo = () => {
  const userInfo = JSON.parse(localStorage.getItem('token'));
  if (!userInfo) return {};
  return `${userInfo.token_type} ${userInfo.access_token}`;
};

export default getUserInfo;
