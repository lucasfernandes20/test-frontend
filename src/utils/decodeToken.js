/* eslint-disable camelcase */
import jwt_decode from 'jwt-decode';

const decodeToken = () => {
  const token = JSON.parse(localStorage.getItem('token'));

  const decoded = jwt_decode(token.access_token);

  return decoded.conta;
};

export default decodeToken;
